package com.kipling.jfinal.plugin.ioc.test;

import com.jfinal.core.Controller;
import com.kipling.jfinal.plugin.ioc.annotation.JFAutowired;

public class TestController extends Controller{
	
	@JFAutowired
	TestJFService jfService;
	
	public void test() {
		System.out.println("TestController.test");
		renderText(jfService.test());
	}
}
