package com.kipling.jfinal.plugin.ioc;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.IPlugin;

import com.kipling.jfinal.plugin.ioc.core.KClassLoader;

/**
 * 
 * @author kipling
 *
 */
public class IocPlugin implements IPlugin {

	String basePackage= "";
	public IocPlugin(String basePackage) {
		this.basePackage = basePackage;
	}
	public boolean start() {
		if(StrKit.isBlank(basePackage)) {
			LogKit.error("包名不能为空");
			return false;
		}
		KClassLoader.init(basePackage);
		KClassLoader.getInstance().classLoader();
		LogKit.info("JFinal IOC Plugin Initial Success.");
		return true;
	}

	public boolean stop() {
		// TODO Auto-generated method stub
		return true;
	}

}
