package com.kipling.jfinal.plugin.ioc.test;

import com.kipling.jfinal.plugin.ioc.core.KClassLoader;

public class TestMain {	

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		//System.out.println(ClassUtil.getClassPath());
		System.out.println(startTime);
		String basePackage = "com.kipling.jfinal.plugin";
		KClassLoader.init(basePackage);
		KClassLoader.getInstance().classLoader();
		System.out.println(System.currentTimeMillis()-startTime);
		TestController test = new TestController();
		test.jfService.test();
	}

}
