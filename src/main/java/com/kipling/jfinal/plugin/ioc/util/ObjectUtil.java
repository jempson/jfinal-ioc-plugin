package com.kipling.jfinal.plugin.ioc.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.jfinal.kit.LogKit;

/**
 * 对象操作工具类
 *
 * @author huangyong
 */
public class ObjectUtil {



    /**
     * 复制所有成员变量
     */
    public static void copyFields(Object source, Object target) {
        try {
            for (Field field : source.getClass().getDeclaredFields()) {
                // 若不为 static 成员变量，则进行复制操作
                if (!Modifier.isStatic(field.getModifiers())) {
                    field.setAccessible(true); // 可操作私有成员变量
                    field.set(target, field.get(source));
                }
            }
        } catch (Exception e) {
        	LogKit.error("复制成员变量出错！", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * 通过反射创建实例
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(String className) {
        T instance;
        try {
            Class<?> commandClass = ClassUtil.loadClass(className);
            instance = (T) commandClass.newInstance();
        } catch (Exception e) {
        	LogKit.error("创建实例出错！", e);
            throw new RuntimeException(e);
        }
        return instance;
    }


}
